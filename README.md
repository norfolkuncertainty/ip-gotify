# ip-gotify

Shell Script to notify when IP changes (Poor mans DDNS)

## Vars

See ip-gotify.conf.example

### GOTIFY_ENDPOINT

Your Gotify endpoint


### GOTIFY_TOKEN

Token for application - obtained from Gotify UI

### IP_FILE

File to write ip address into

### IDENTIFIER

Helpful Identifier incase you are tracking multiple IPs

## Usage

### Basic

```
./ip-gotify.sh ip-gotify.conf
```

### Systemd Timer

- Copy the files from the systemd folder into /etc/systemd/system
- Copy ip-gotify.sh into /usr/local/bin
- Copy ip-gotify.conf into /etc
- Run `systemctl daemon-reload`

## Tips

Do not run this as root

## Thanks

- Thanks to ipinfo.io for providing their service
- Thanks to the Gotify team for supplying a great selfhostable service