#! /usr/bin/env bash

source $1
touch $IP_FILE

CURRENT_IP=$(curl -s https://ipinfo.io/ip)
CACHED_IP=$(cat ${IP_FILE})

if [ "${CURRENT_IP}" != "${CACHED_IP}" ]; then
  curl -s -X POST "${GOTIFY_ENDPOINT}/message?token=${GOTIFY_TOKEN}" -F "title=${IDENTIFIER} IP Change" -F "message=New IP is ${CURRENT_IP}" -F "priority=5"
  echo ${CURRENT_IP} > ${IP_FILE}
fi